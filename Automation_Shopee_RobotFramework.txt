
* Settings *
Library       	Selenium2Library
 
* Variables *



${user_git}         	xpath=//*[@id="login_field"]
${password_git}    		xpath=//*[@id="password"]
${login}				xpath=//*[@id="login"]/form/div[3]/input[3]
${click_pivot}  	  	xpath=//*[@id="user-links"]/li[3]/details/summary/span
${Yr_gist}  		    xpath=//*[@id="user-links"]/li[3]/details/details-menu/ul/li[6]/a
${input}                xpath=//*[@id="gists"]/div[2]/div/div[1]/div[2]/input[2]
${fill_body}            xpath=//*[@id="gists"]/div[2]/div/div[2]/div/div[5]
${create_gist}          xpath=//*[@id="new_gist"]/div/div[2]/button[2]

${browser}  			chrome
 
* Keywords *
 
 
* Test Cases *
Valid User Login
	Open Browser	https://github.com/login  chrome

	#Sleep    10
	#Page Should Contain Element	${hidden submenu}
    Sleep 								5
	Maximize Browser Window



  Input Text				        ${user_git}            maulanajayadi137
  Input Text	                    ${password_git} 	   M4ul4n4123
  Click Element 					${login}
  Click Element 				    ${click_pivot}           
  Sleep       						        5
  Click Element                      ${Yr_gist} 
  Sleep       					       	10
	Input Text			             ${input}              create-automate-shopee    	
	Input Text  	    	         ${fill_body}          Test Data 
	Click Element	                 ${create_gist}
 	Capture Page Screenshot
 	Sleep                           10
    Close Browser


/* Deleted-gist */

* Settings *
Library       	Selenium2Library
 
* Variables *



${button_Edit}         	xpath=//*[@id="gist-pjax-container"]/div[1]/div/div[1]/ul/li[1]/a
${button_Deleted}       xpath=//*[@id="gists"]/div[2]/div[2]/div[1]/div[2]/span/button
${browser}  			chrome
 
* Keywords *
 
 
* Test Cases *
Valid User Login
	Open Browser	https://github.com/login  chrome

	#Sleep    10
	#Page Should Contain Element	${hidden submenu}
    Sleep 								5
	Maximize Browser Window
	Click Element					${button_Edit}  
    Click Element 					${button_Deleted}
 	Capture Page Screenshot
 	Sleep                           10
    Close Browser


/*Edit-gist*/

* Settings *
Library       	Selenium2Library
 
* Variables *



${button_Edit}         	xpath=//*[@id="gist-pjax-container"]/div[1]/div/div[1]/ul/li[1]/a
${input}                xpath=//*[@id="gists"]/div[2]/div[2]/div[1]/div[2]/input[2]
${fill_body}            xpath=//*[@id="gists"]/div[2]/div/div[2]/div/div[5]
${update_secret}        xpath=//*[@id="edit_gist_91141189"]/div/div[2]/button



${browser}  			chrome
 
* Keywords *
 
 
* Test Cases *
Valid User Login
	Open Browser	https://github.com/login  chrome

	#Sleep    10
	#Page Should Contain Element	${hidden submenu}
    Sleep 								5
	Maximize Browser Window
	Sleep       						5
	Input Text						${input}              Edit-gist-automation-shopee  	
	Input Text  					${fill_body}          Test Data Edit
	Click Element	                ${update_secret}
 	Capture Page Screenshot
 	Sleep                           10
    Close Browser


/* see-all-your-gits*/

* Settings *
Library       	Selenium2Library
 
* Variables *



${user_git}         	xpath=//*[@id="login_field"]
${password_git}    		xpath=//*[@id="password"]
${login}				xpath=//*[@id="login"]/form/div[3]/input[3]
${click_pivot}  		xpath=//*[@id="user-links"]/li[3]/details/summary/span
${Yr_gist}  		    xpath=//*[@id="user-links"]/li[3]/details/details-menu/ul/li[6]/a
${button_see_all_your_gits}         	xpath=//*[@id="gist-pjax-container"]/div[1]/div/div/ul/li[2]

${browser}  			chrome
 
* Keywords *
 
 
* Test Cases *
Valid User Login
	Open Browser	https://github.com/login  chrome

	#Sleep    10
	#Page Should Contain Element	${hidden submenu}
    Sleep 								5
	Maximize Browser Window



   Click Element 					$${button_see_all_your_gits} 
   
 	Capture Page Screenshot
 	Sleep                           10
    Close Browser

