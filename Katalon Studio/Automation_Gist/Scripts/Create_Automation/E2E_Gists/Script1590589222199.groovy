import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://github.com/login')
WebUI.maximizeWindow()

WebUI.delay(2)
WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Sign in to GitHub  GitHub/input_Username or email address_login'), 
    'maulanajayadi137@gmail.com')

WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Sign in to GitHub  GitHub/input_Forgot password_password'), 
    'M4ul4n4123')

WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Sign in to GitHub  GitHub/input_Forgot password_commit'))


WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_GitHub/summary_Sign out_Header-link'))

WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_GitHub/a_New gist'))

WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create a new Gist/input_See all of your gists_gistdescription'), 
    'Create E2E Gist')

WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create a new Gist/input_See all of your gists_gistcontentsname'), 
    'E2E Gist')

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create a new Gist/div_1'))

WebUI.delay(2)
WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create a new Gist/div_1Create Gist'), '<div style="position: relative;"><div class="CodeMirror-gutter-wrapper" contenteditable="false" style="left: -53px;"><div class="CodeMirror-linenumber CodeMirror-gutter-elt" style="left: 0px; width: 21px;">1</div></div><pre class=" CodeMirror-line " role="presentation"><span role="presentation" style="padding-right: 0.1px;">Create Gist</span></pre></div>')

WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create a new Gist/button_Create public gist'))
//Edit Gist

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create E2E Gist/a_Edit'))
//Add File button

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/button_Add file'))

WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/input_Create Gist_gistcontentsname'), 
    'Add the data Gist 1')

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/pre_'))

WebUI.delay(2)
WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/div_1Add the data'), '<div style="position: relative;"><div class="CodeMirror-gutter-wrapper" contenteditable="false" style="left: -53px;"><div class="CodeMirror-linenumber CodeMirror-gutter-elt" style="left: 0px; width: 21px;">1</div></div><pre class=" CodeMirror-line " role="presentation"><span role="presentation" style="padding-right: 0.1px;">Add the data </span></pre></div>')
//Add File button
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/button_Add file'))

WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/input_Create Gist_gistcontentsname'), 
    'Add the data Gist 2')

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/div_1'))

WebUI.setText(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/div_1Add the data 2'), '<div style="position: relative;"><div class="CodeMirror-gutter-wrapper" contenteditable="false" style="left: -53px;"><div class="CodeMirror-linenumber CodeMirror-gutter-elt" style="left: 0px; width: 21px;">1</div></div><pre class=" CodeMirror-line " role="presentation"><span role="presentation" style="padding-right: 0.1px;">Add the data 2</span></pre></div>')

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing E2E Gist/button_Update public gist'))

WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create E2E Gist/a_Edit'))

WebUI.delay(2)
//Deleted Gists
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing Add the data Gist 1/path'))

WebUI.delay(2)
//updated Gists
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Editing Add the data Gist 1/button_Update public gist'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create E2E Gist/svg_Back to GitHub_octicon octicon-plus'))
//See All Yr Gists
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create a new Gist/a_See all of your gists'))

WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_maulanajayadi137s gists/strong_Add the data Gist 1'))

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create E2E Gist/summary_Back to GitHub_Header-link name'))

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Create E2E Gist/button_Sign out'))

WebUI.click(findTestObject('Object Repository/E2E_Gists_Fix/Page_Sign out/input_Are you sure you want to sign out_btn_960785'))

